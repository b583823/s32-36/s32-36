const Course = require("../models/Course");
// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body.
	2. Save the new User to the database.

*/

module.exports.addCourse = (reqBody) =>{
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})
	return newCourse.save().then((course,error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}
// Retrieve all courses
/*
	Step:
	1. Retrieve all the courses from the database

*/

module.exports.getAllCourses = () =>{
	return Course.find({}).then(result=>result);
}
// Retrieve all Active Courses
/*
	Steps:
	1. Retrieve all courses from the database with property of "isActive" to true.
*/

module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result=>result);
}

module.exports.getCourse = (courseId) =>{
	return Course.findById(courseId).then(result=>result);
}

// Update a course
/*
	Steps:
	1. Create a variable "updateCourse" wchjich will contain the information retrieved from the request body.
	2. Find and update the course using the courseId retrieved from request params/url property and the variable "updatedCourse" containing the information from the request body.
*/

module.exports.updateCourse = (courseId,reqBody) =>{
	let updatedCourse ={
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	}
	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdate, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}
module.exports.archiveCourse = (courseId) =>{

	let updateStatus= {
		isActive: false
	}
	return Course.findByIdAndUpdate(courseId,updateStatus).then((result,error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}