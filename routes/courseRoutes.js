const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth")
// Route for creating a course
// router.post("/", (req,res)=>{

// 	courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// })

router.post("/", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization); //contains the token
	console.log(userData);

	if(userData.isAdmin){
	courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}
	else{
		return res.send("You are not allowed to this page");
	}

})
// Route for retrieving all the courses
router.get("/all", auth.verify, (req,res)=>{
	courseControllers.getAllCourses().then(resultFromController=>res.send(resultFromController));
})
// Router for retrieving all active courses
router.get("/",(req,res)=>{
	courseControllers.getAllActive().then(resultFromController=>res.send(resultFromController))
})
router.get("/:courseId",(req, res)=>{
	console.log(req.params.courseId);
	courseControllers.getCourse(req.params.courseId).then(resultFromController=>res.send(resultFromController));
})
router.put("/:courseId", auth.verify,(req,res)=>{
	courseControllers.updateCourse(req.params.courseId, req.body).then(resultFromController=>res.send(resultFromController));
})
// Route to archive a course

router.patch("/:courseId/archive",  auth.verify, (req,res)=>{
	courseControllers.archiveCourse(req.params.courseId).then(resultFromController=>res.send(resultFromController));
})
module.exports = router;