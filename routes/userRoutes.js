const express = require("express");
const router = express.Router();
const auth = require("../auth")
const userControllers = require("../controllers/userControllers");

// Router for checking if the email exists
router.post("/checkEmail",(req, res)=>{
	userControllers.checkEmailExists(req.body).then(resultFromController =>res.send(resultFromController));
})

router.post("/register",(req,res)=>{
	userControllers.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

// Route for the user login (with token creation)
router.post("/login",(req,res)=>{
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/details", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization); //contains the token
	console.log(userData);
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

router.post("/enroll", auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization)
	let data = {
		userId: userData.id,
		// courseId will be retrieved from the request body
		courseId: req.body.courseId
	}
	if(userData.isAdmin){
		res.send("You're not allowed to access this page")
	}
	else{
	userControllers.enroll(data).then(resultFromController=> res.send(resultFromController))
	}
})

module.exports = router;
