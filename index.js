// Require modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const courseRoutes = require("./routes/courseRoutes");
const userRoutes = require("./routes/userRoutes");
// Server
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin@zuittbootcamp.e1kfs3u.mongodb.net/course-booking-app?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

// Set notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Router for our API
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


// Listening to port
//  This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || port, ()=>{
	console.log(`API is now online on port ${process.env.PORT|| port}`)
});

